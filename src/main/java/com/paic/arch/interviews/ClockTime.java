package com.paic.arch.interviews;

import static com.paic.arch.interviews.Utils.checkParam;

/**
 * ClockTime
 *
 * @author Chenpz
 * @package com.paic.arch.interviews
 * @date 2018/3/21/23:22
 */
public class ClockTime implements TimeConverter {
    
    private TimeConverter hourTime;
    private TimeConverter minTime;
    private TimeConverter secTime;
    
    public ClockTime(){
        hourTime = new HourTime();
        minTime  = new MinTime();
        secTime  = new SecTime();
    }

    @Override
    public String convertTime(String aTime) {

        // 校验输入是否合法
        checkParam(aTime);
        // 使用 : 分割 获取 时/分/秒 数组
        String[] aTimeArr = aTime.split(":");

        return new StringBuilder()
                .append(secTime.convertTime(aTimeArr[2]))
                .append(hourTime.convertTime(aTimeArr[0]))
                .append(minTime.convertTime(aTimeArr[1]))
                .toString();
    }
}
