package com.paic.arch.interviews;

/**
 * HourTime
 *  小时灯 转换
 * @author Chenpz
 * @package com.paic.arch.interviews
 * @date 2018/3/21/23:17
 */
public class HourTime implements TimeConverter {

    @Override
    public String convertTime(String aTime) {
        Integer hour = Integer.valueOf(aTime);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(convertTime(hour / 5))
                .append("\r\n")
                .append(convertTime(hour % 5))
                .append("\r\n");
        return stringBuilder.toString();
    }
    
    private String convertTime(Integer caseFlag){
        switch (caseFlag){
            case 0:
                return "OOOO";
            case 1:
                return "ROOO";
            case 2:
                return "RROO";
            case 3:
                return "RRRO";
            case 4:
                return "RRRR";
            default:
                    throw new RuntimeException("convertTime Exception");
        }
        
    }
    
    
}
