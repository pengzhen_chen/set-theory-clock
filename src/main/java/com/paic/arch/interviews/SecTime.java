package com.paic.arch.interviews;

/**
 * SecTime
 * 秒灯转换
 * @author Chenpz
 * @package com.paic.arch.interviews
 * @date 2018/3/21/23:42
 */
public class SecTime implements TimeConverter {

    @Override
    public String convertTime(String aTime) {
        StringBuilder sb = new StringBuilder();
        Integer sec = Integer.valueOf(aTime);
        if (sec % 2 == 0){
            sb.append("Y");
        }else{
            sb.append("O");
        }
        sb.append("\r\n");
        return sb.toString();
    }
}
