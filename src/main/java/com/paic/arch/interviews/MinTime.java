package com.paic.arch.interviews;

/**
 * MinTime
 * 分钟灯 转换
 * @author Chenpz
 * @package com.paic.arch.interviews
 * @date 2018/3/21/23:19
 */
public class MinTime implements TimeConverter {
    
    @Override
    public String convertTime(String aTime) {
        Integer min = Integer.valueOf(aTime);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(converTime(min / 5))
                .append("\r\n")
                .append(coverMinTime(min % 5));
        return stringBuilder.toString();
    }
    
    
    private String converTime(Integer quotientNum){
        
        switch (quotientNum){
            case 0:
                return "OOOOOOOOOOO";
            case 1:
                return "YOOOOOOOOOO";
            case 2:
                return "YYOOOOOOOOO";
            case 3:
                return "YYROOOOOOOO";
            case 4:
                return "YYRYOOOOOOO";
            case 5:
                return "YYRYYOOOOOO";
            case 6:
                return "YYRYYROOOOO";
            case 7:
                return "YYRYYRYOOOO";
            case 8:
                return "YYRYYRYYOOO";
            case 9:
                return "YYRYYRYYYOO";
            case 10:
                return "YYRYYRYYRYO";
            case 11:
                return "YYRYYRYYRYY";
            default:
                throw new RuntimeException("Min TimeConverter Error!");
            
        }
    }
    
    private String coverMinTime(Integer modNum){
        switch (modNum){
            case 0:
                return "OOOO";
            case 1:
                return "YOOO";
            case 2:
                return "YYOO";
            case 3:
                return "YYYO";
            case 4:
                return "YYYY";
            default:
                throw new RuntimeException("Min TimeConverter Error!");
        
        }
    
    }
    
}
