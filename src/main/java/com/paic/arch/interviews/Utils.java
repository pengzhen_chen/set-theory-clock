package com.paic.arch.interviews;

import java.util.regex.Pattern;

/**
 * Utils
 *
 * @author Chenpz
 * @package com.paic.arch.interviews
 * @date 2018/3/21/23:27
 */
public class Utils {

    
    public static void checkParam(String timeClock){
        String timePattern = "([01]?[0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9]";
        Pattern pattern = Pattern.compile(timePattern);
        if (timeClock == null || !pattern.matcher(timeClock).matches()){
            throw new RuntimeException("the input illegal!");
        }
    }

}
